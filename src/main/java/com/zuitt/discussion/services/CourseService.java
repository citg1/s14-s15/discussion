package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    //    Create a course
    void createCourse(String stringToken, Course course);
    //    Viewing all courses
    Iterable<Course> getCourses();
    //    Delete a course
    ResponseEntity deleteCourse(Long id, String stringToken);

    //    Update a courses
    ResponseEntity updateCourse(Long id, String stringToken, Course course);

    //    To get all course of a specific user
    Iterable<Course> getMyCourses(String stringToken);
}
